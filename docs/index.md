## index
Javascript interface for the XML Split tool


* [index](#markdown-header-index)
    * [module.exports.split(source, node, options)](#markdown-header-moduleexportssplitsource-node-options-promise) ⇒ Promise ⏏
        * [~splitter](#markdown-header-moduleexportssplitsplitter-srcxmlsplitter) : src/xmlsplitter

### module.exports.split(source, node, options) ⇒ Promise ⏏
Javascript interface method to split large XML files

**Kind**: Exported function  
**Resolve**: Integer If everything goes well, returns the number of chunk files generated  
**Reject**: Error Returns an error if one happened  

| Param | Type | Description |
| --- | --- | --- |
| source | String | The source file path where to find the big XML file to split |
| node | String | The node that represent the biggest number of nodes (that we want to move into chunks) |
| options | Object | The object that will contains all options that can be passed to the method (to simulate the commnad line options) |

**Example**  
```js
// This command will split the XML file into chunks of 10 Mb files, renamed as file_part_0.xml, file_part_1.xml, file_part_2.xml ...
const xmlSplit = require('xmlsplit');

xmlSplit.split('path/to/big/xml/file.xml', 'child_node', {
    size: 10,
    pattern: '_part_',
    counter: 0
});
```
#### module.exports.split~splitter : src/xmlsplitter
**Kind**: inner constant of module.exports.split  
