## cli
Command line interface for the XML Split tool

Usage: split [options] <source> <node>

Options:
*   -s, --size [size]        Size of the target file. The size has to be a Mb number (i.e. "100" will be interpreted as 100Mb). Default: 10.
*   -p, --pattern [pattern]  Pattern appended at the end of the file names (before the file extention).
*   -c, --counter [counter]  Starting value of the file counter. Default: 1
*   -h, --help               output usage information

The following examples will split the XML file into chunks of 10 Mb files, renamed as file.part_0.xml, file.part_1.xml, file.part_2.xml ...

**Example**  
```js
// with node command
node bin/cli.js split path/to/big/xml/file.xml child_node -s 10 -p ".part_" -c 0
```
**Example**  
```js
// after a "npm link"
xmlsplit split path/to/big/xml/file.xml child_node -s 10 -p ".part_" -c 0
```

* [cli](#markdown-header-cli)
    * [~packageDetails](#markdown-header-clipackagedetails-object) : Object
    * [~program](#markdown-header-cliprogram-node_modulescommander) : node_modules/commander
    * [~log](#markdown-header-clilog-liblog) : lib/log
    * [~splitter](#markdown-header-clisplitter-srcxmlsplitter) : src/xmlsplitter

### cli~packageDetails : Object
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~program : node_modules/commander
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~log : lib/log
**Kind**: inner constant of [cli](#markdown-header-cli)  
### cli~splitter : src/xmlsplitter
**Kind**: inner constant of [cli](#markdown-header-cli)  
