## Modules
Module | Description
------ | -----------
[helper](#markdown-header-helper) | Helper toolbox
[log](#markdown-header-log) | Custom logger
[xmlstreamer](#markdown-header-xmlstreamer) | Main XML Stream handler

## helper
Helper toolbox


* [helper](#markdown-header-helper)
    * [~MB_TO_B_MULTIPLIER](#markdown-header-helpermb_to_b_multiplier-integer) : Integer
    * [~DEFAULTS](#markdown-header-helperdefaults-object) : Object
    * [~parseCounter(opts)](#markdown-header-helperparsecounteropts-int) ⇒ Int
    * [~parsePattern(opts)](#markdown-header-helperparsepatternopts-string) ⇒ String
    * [~parseSize(opts)](#markdown-header-helperparsesizeopts-int) ⇒ Int

### helper~MB_TO_B_MULTIPLIER : Integer
**Kind**: inner constant of [helper](#markdown-header-helper)  
### helper~DEFAULTS : Object
**Kind**: inner constant of [helper](#markdown-header-helper)  
### helper~parseCounter(opts) ⇒ Int
Parse the counter option

**Kind**: inner method of [helper](#markdown-header-helper)  

| Param | Type |
| --- | --- |
| opts | Object | 

### helper~parsePattern(opts) ⇒ String
Parse the pattern option

**Kind**: inner method of [helper](#markdown-header-helper)  

| Param | Type |
| --- | --- |
| opts | Object | 

### helper~parseSize(opts) ⇒ Int
Parse the size option

**Kind**: inner method of [helper](#markdown-header-helper)  

| Param | Type |
| --- | --- |
| opts | Object | 

## log
Custom logger


* [log](#markdown-header-log)
    * [info](#markdown-header-info-maybe) ⇒ Maybe ⏏
    * [warn](#markdown-header-warn-maybe) ⇒ Maybe ⏏
    * [success](#markdown-header-success-maybe) ⇒ Maybe ⏏
    * [error](#markdown-header-error-maybe) ⇒ Maybe ⏏
    * [isQuiet(checkOnlyRunningMode)](#markdown-header-isquietcheckonlyrunningmode-boolean) ⇒ Boolean ⏏

### info ⇒ Maybe ⏏
Log the given message as an info message

**Kind**: Exported member  
### warn ⇒ Maybe ⏏
Log the given message as a warn message

**Kind**: Exported member  
### success ⇒ Maybe ⏏
Log the given message as a success message

**Kind**: Exported member  
### error ⇒ Maybe ⏏
Log the given message as an error message

**Kind**: Exported member  
### isQuiet(checkOnlyRunningMode) ⇒ Boolean ⏏
Returns true if the tool has to be quiet (meaning not logging everything)

**Kind**: Exported function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| checkOnlyRunningMode | Boolean | `false` | only checks the running mode (not the environment variables) |

## xmlstreamer
Main XML Stream handler


* [xmlstreamer](#markdown-header-xmlstreamer)
    * [~EventEmitter](#markdown-header-xmlstreamereventemitter-node_moduleseventseventemitter) : node_modules/events/EventEmitter
    * [~fs](#markdown-header-xmlstreamerfs-node_modulesfs) : node_modules/fs
    * [~util](#markdown-header-xmlstreamerutil-node_modulesutil) : node_modules/util
    * [~Writable](#markdown-header-xmlstreamerwritable-node_modulesstreamwritable) : node_modules/stream/Writable
    * [~log](#markdown-header-xmlstreamerlog-liblog) : lib/log
    * [~ENCODING](#markdown-header-xmlstreamerencoding-string) : String
    * [~REGEXP_XML_ELEMENT](#markdown-header-xmlstreamerregexp_xml_element-regexp) : RegExp

### xmlstreamer~EventEmitter : node_modules/events/EventEmitter
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~fs : node_modules/fs
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~util : node_modules/util
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~Writable : node_modules/stream/Writable
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~log : lib/log
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~ENCODING : String
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
### xmlstreamer~REGEXP_XML_ELEMENT : RegExp
**Kind**: inner constant of [xmlstreamer](#markdown-header-xmlstreamer)  
