## xmlsplitter
Main XML Splitter handle


* [xmlsplitter](#markdown-header-xmlsplitter)
    * [split(sourceFile, node, options)](#markdown-header-splitsourcefile-node-options-promise) ⇒ Promise ⏏
        * [~fs](#markdown-header-splitfs-node_modulesfs) : node_modules/fs
        * [~path](#markdown-header-splitpath-node_modulespath) : node_modules/path
        * [~helper](#markdown-header-splithelper-libhelper) : lib/helper
        * [~log](#markdown-header-splitlog-liblog) : lib/log
        * [~XmlStreamer](#markdown-header-splitxmlstreamer-libstreamer) : lib/streamer

### split(sourceFile, node, options) ⇒ Promise ⏏
The split method will generate small chuncks of the given {sourceFile} based on the given {options}

**Kind**: Exported function  
**Resolve**: Integer If everything goes well, returns the number of chunk files generated  
**Reject**: Error Returns an error if one happened  

| Param | Type | Description |
| --- | --- | --- |
| sourceFile | String | The path of the source path |
| node | String | The node that represent the large number of items |
| options | Object | Options from commander |

#### split~fs : node_modules/fs
**Kind**: inner constant of split  
#### split~path : node_modules/path
**Kind**: inner constant of split  
#### split~helper : lib/helper
**Kind**: inner constant of split  
#### split~log : lib/log
**Kind**: inner constant of split  
#### split~XmlStreamer : lib/streamer
**Kind**: inner constant of split  
