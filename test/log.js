'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;

/**
 * @type {lib/log}
 */
const log = require('../lib/log');

describe('lib/log.js', () => {
    describe('Custom logger methods', () => {
        it('test with full quiet mode', () => {
            process.env.quiet = true;
            process.env.runningAsScript = true;

            expect(log.isQuiet()).to.equal(true);
            expect(log.isQuiet(true)).to.equal(true);
            expect(log.info('info logged').isNothing()).to.equal(true);
            expect(log.warn('warn logged').isNothing()).to.equal(true);
            expect(log.success('success logged').isNothing()).to.equal(true);
            expect(log.error('error logged').isNothing()).to.equal(true);
        });

        it('test without quiet mode but running as script', () => {
            process.env.quiet = undefined;
            process.env.runningAsScript = true;

            expect(log.isQuiet()).to.equal(true);
            expect(log.isQuiet(true)).to.equal(true);
            expect(log.info('info logged').isNothing()).to.equal(true);
            expect(log.warn('warn logged').isNothing()).to.equal(true);
            expect(log.success('success logged').isNothing()).to.equal(true);
            expect(log.error('error logged').isNothing()).to.equal(true);
        });

        it('test with quiet mode but without running as script', () => {
            process.env.quiet = true;
            process.env.runningAsScript = undefined;

            expect(log.isQuiet()).to.equal(true);
            expect(log.isQuiet(true)).to.equal(false);
            expect(log.info('info logged').isNothing()).to.equal(true);
            expect(log.warn('warn logged').isNothing()).to.equal(true);
            expect(log.success('success logged').isJust()).to.equal(true);
            expect(log.error('error logged').isJust()).to.equal(true);
        });

        it('test without quiet mode', () => {
            process.env.quiet = undefined;
            process.env.runningAsScript = undefined;

            expect(log.isQuiet()).to.equal(false);
            expect(log.isQuiet(true)).to.equal(false);
            expect(log.info('info logged').isJust()).to.equal(true);
            expect(log.warn('warn logged').isJust()).to.equal(true);
            expect(log.success('success logged').isJust()).to.equal(true);
            expect(log.error('error logged').isJust()).to.equal(true);
        });
    });
});
