'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;

/**
 * @type {src/xmlsplit}
 */
const xmlsplit = require('../src/xmlsplit');

describe('src/xmlsplit.js', () => {
    describe('Split method - rejected promise', () => {
        it('Without any parameters', () => {
            return xmlsplit.split().then(res => {
                throw new Error(res);
            }, res => {
                expect(res.message).to.deep.equal('Missing input parameters.');
            });
        });

        it('Without node parameter', async () => {
            return xmlsplit.split('').then(res => {
                throw new Error(res);
            }, res => {
                expect(res.message).to.deep.equal('Missing input parameters.');
            });
        });

        it('Empty input parameters', async () => {
            return xmlsplit.split('', '').then(res => {
                throw new Error(res);
            }, res => {
                expect(res.message).to.deep.equal('Missing input parameters.');
            });
        });

        it('Bad source file parameter', async () => {
            return xmlsplit.split('bad/path/to/file.xml', 'test').then(res => {
                throw new Error(res);
            }, res => {
                expect(res.message).to.deep.equal('The source file "bad/path/to/file.xml" does not exists.');
            });
        });
    });

    describe('Split method - resolving promise', () => {
        it('Split the resource file', () => {
            process.env.runningAsScript = true;
            process.env.quiet = true;
            return xmlsplit.split('resources/test-file.xml', 'document', {
                size: 0.001,
                pattern: '.part_'
            }).then(res => {
                expect(res).to.equal(3);
            });
        });
    });
});
