'use strict';

/**
 * @type {node_modules/chai.expect}
 */
const expect = require('chai').expect;

/**
 * @type {lib/helper}
 */
const helper = require('../lib/helper');

/**
 * @type Object
 */
const OPTS = {
    COMMANDER: {
        commands: [],
        options: [{
            flags: '-s, --size [size]',
            required: 0,
            optional: -12,
            bool: true,
            short: '-s',
            long: '--size',
            description: 'Size of the target file. The size has to be a Mb number (i.e. "100" will be interpreted as 100Mb). Default: 10.'
        }, {
            flags: '-p, --pattern [pattern]',
            required: 0,
            optional: -15,
            bool: true,
            short: '-p',
            long: '--pattern',
            description: 'Pattern appended at the end of the file names (before the file extention).'
        }, {
            flags: '-c, --counter [counter]',
            required: 0,
            optional: -15,
            bool: true,
            short: '-c',
            long: '--counter',
            description: 'Starting value of the file counter. Default: 1'
        }, {
            flags: '-r, --removecomments',
            required: 0,
            optional: -15,
            bool: true,
            short: '-r',
            long: '--removecomments',
            description: 'Remove comments from the XML file prior to split it.'
        }],
        _execs: {},
        _allowUnknownOption: false,
        _args: [
            { required: true, name: 'source', variadic: false },
            { required: true, name: 'node', variadic: false }
        ],
        _name: 'split',
        _noHelp: false,
        parent: {
            commands: [[{}]],
            options: [[{}]],
            _execs: {},
            _allowUnknownOption: false,
            _args: [],
            _name: 'cli',
            Command: { super_: {} },
            Option: [{}],
            _version: '1.0.4',
            _versionOptionName: 'version',
            _events: {
                'option:version': {},
                'command:split': {}
            },
            _eventsCount: 2,
            _description: 'Split a large XML file into smaller chunks based on the given options',
            _argsDescription: undefined,
            rawArgs: [
                '/usr/local/bin/node',
                'path/to/cli.js',
                'split',
                '/path/to/large/file.xml',
                'node',
                '-s',
                '5',
                '-p',
                '.part_',
                '-c',
                '0'
            ],
            args: [
                '/path/to/large/file.xml',
                'node',
                []
            ]
        },
        _events: {
            'option:size': [],
            'option:pattern': [],
            'option:counter': []
        },
        _eventsCount: 3,
        size: '5',
        pattern: '.part_',
        counter: '0',
        removecomments: true
    },
    JS: {
        size: 5,
        pattern: '.part_',
        counter: 0,
        removecomments: true
    },
    JSWITHNAN: {
        size: 'sss',
        pattern: undefined,
        counter: 'ccc'
    }
};

/**
 * @type Object
 */
const EXPECTED = {
    DEFAULT: {
        counter: 1,
        pattern: '',
        size: 10000000,
        removecomments: false
    },
    WITHINPUTS: {
        counter: 0,
        pattern: '.part_',
        size: 5000000,
        removecomments: true
    }
};

describe('lib/helper.js', () => {
    describe('Command Line interface options parsing', () => {
        it('Parse CLI default options', () => {
            var opts = helper.parseOptions(undefined);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT);
        });

        it('Parse CLI options', () => {
            var opts = helper.parseOptions(OPTS.COMMANDER);
            expect(opts).to.deep.equal(EXPECTED.WITHINPUTS);
        });
    });

    describe('Javascript interface options parsing', () => {
        it('Parse JS default options', () => {
            var opts = helper.parseOptions(undefined);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT);
        });

        it('Parse JS options', () => {
            var opts = helper.parseOptions(OPTS.JS);
            expect(opts).to.deep.equal(EXPECTED.WITHINPUTS);
        });

        it('Parse JS options with NaN', () => {
            var opts = helper.parseOptions(OPTS.JSWITHNAN);
            expect(opts).to.deep.equal(EXPECTED.DEFAULT);
        });
    });
});
