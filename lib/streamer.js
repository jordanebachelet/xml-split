'use strict';

/**
 * Main XML Stream handler
 * @module xmlstreamer
 */

/**
 * @type {node_modules/events/EventEmitter}
 */
const EventEmitter = require('events').EventEmitter;
/**
 * @type {node_modules/fs}
 */
const fs = require('fs');
/**
 * @type {node_modules/util}
 */
const util = require('util');
/**
 * @type {node_modules/stream/Writable}
 */
const Writable = require('stream').Writable;

/**
 * @type {lib/log}
 */
const log = require('./log');

/**
 * @type String
 */
const ENCODING = 'UTF-8';

/**
 * @type RegExp
 */
const REGEXP_XML_ELEMENT = /<([\w:_-]+)[>\s]/mg;

// transforms a big XML file into smaller chunks of valid xml documents
util.inherits(XmlStream, Writable);

/**
 * Class definition of the XmlStream writable function
 *
 * @alias module:xmlstreamer/XmlStream
 * @constructor
 * @extends {node_modules/stream/Writable}
 *
 * @param {Integer} batchSize The size of the generated chunk files
 * @param {String} tagName The XML tag on which split the big files
 * @param {String} filepath The absolute path where to put generated files
 * @param {String} filename The source file name
 * @param {String} pattern The pattern to append to generated file names
 * @param {String} extention The source file extention to use for generated files
 * @param {Integer} counter The first index of file counter
 * @param {Object} options The options to send to the parent Writable class
 */
function XmlStream(batchSize, tagName, filepath, filename, pattern, extention, counter, options) {
    Writable.call(this, options);

    this._header = null;
    this._footerTag = null;
    this._tag = null;
    this._bufferSize = 0;
    this._data = '';
    this._buffer = [];
    this._batchSize = batchSize;
    this._tagName = tagName;

    this._filepath = filepath;
    this._filename = filename;
    this._pattern = pattern;
    this._extention = extention;
    this._fileCounter = counter;
    this._writer = null;
    this._files = [];
    this._endedFiles = 0;

    /**
     * Return the number of found open tags for the given {tag} in the given {text}
     *
     * @param {String} tagName
     * @param {String} text
     *
     * @returns {Integer}
     */
    this.countOpenTags = (tagName, text) => {
        var count = 0;
        var re = new RegExp('<\\s*' + tagName + '[\\s+>]', 'mgi');
        while (re.exec(text)) {
            count++;
        }
        return count;
    };

    /**
     * This method will end the actual file generation and start another file.
     * If the fileWriter does not exist yet, just create a new file
     *
     * @alias module:streamer
     *
     * @param {Boolean} startNewFile
     */
    this.endAndStartFile = startNewFile => {
        if (this._writer !== null) {
            this._writer.end(this._footerTag);
            this._bufferSize = 0;
            this._endedFiles++;
        }

        if (startNewFile !== undefined && startNewFile !== true) {
            return;
        }

        var filePath = [
            this._filepath,
            this._filename,
            this._pattern,
            this._fileCounter,
            this._extention
        ].join('');

        this._files.push(filePath);
        this._writer = fs.createWriteStream(filePath);

        log.success(`Opening the file "${filePath}".`);

        this._writer.on('finish', () => {
            log.success(`File "${filePath}" successfully generated.`);

            if (this._endedFiles === this._files.length) {
                this.emit('done', this._endedFiles);
            }
        });

        if (this._header !== undefined) {
            this._writer.write(this._header);
        }

        this._fileCounter++;
    };
}

/**
 * This method is called at the end of the process
 * When the inputStream is finished and the Writable class has finished also.
 * It is used to close the latest generated file
 * It simulates the {_flush} method of a Transform streamable
 *
 * @param {Function} next
 */
XmlStream.prototype._flush = function (next) {
    if (this._buffer.length > 0) {
        this.endAndStartFile(false);

        this._buffer = [];
        this._bufferSize = 0;
    } else {
        this.endAndStartFile(false);
    }

    next();
};

/**
 * This method is called everytime the Writable class is reading a new chunk from the input stream.
 * It will handle the XML header/footer and correct tags open/close tags and write that in chunk files.
 *
 * @param {Buffer} chunk
 * @param {String} encoding
 * @param {Function} next
 */
XmlStream.prototype._write = function (chunk, encoding, next) {
    var res;
    var tagName;
    this._data += chunk.toString();

    // check if we have open xml comments fragments, then wait for the next chunk
    if (this._data.match(/<!--/)) {
        return next();
    }


    if (!this._header) {
        // check if we have the full xml header in the input this._data
        res = (REGEXP_XML_ELEMENT).exec(this._data);
        if (res !== null) {
            tagName = res[1];
            this._footerTag = '</' + tagName + '>';
            this._header = this._data.slice(0, res.index + 1);
            this._data = this._data.slice(res.index + 1);
        }
    }

    // process the this._data if available
    if (!this._header || !this._data) {
        return next();
    }

    if (!this._tag) {
        // if there is a preconfigured tag, try to find that one, else autodetect
        var re = this._tagName ? new RegExp('<(' + this._tagName + ')', 'mgi') : REGEXP_XML_ELEMENT;

        res = re.exec(this._data);
        if (res !== null) {
            tagName = res[1];
            this._tag = tagName;
            this._header += this._data.slice(0, res.index);
            this._data = this._data.slice(res.index);
        }
    }

    if (!this._tag || !this._data) {
        return next();
    }

    var endTag = '</' + this._tag + '>';
    var dataChunks = this._data.split(endTag);

    if (dataChunks.length > 1) {
        var lastChunkIndex = dataChunks.length - 1;
        var dataChunk = '';
        var closeTagCount = 0;

        dataChunks.forEach((data, index) => {
            dataChunk += data;
            closeTagCount++;

            if (index === lastChunkIndex) {
                this._data = dataChunk;
            } else {
                // Count the open tags inside the chunk and check if they do match with the closing ones
                if (this.countOpenTags(this._tag, dataChunk) !== closeTagCount) {
                    // If not append the closing tag to the dataChunk and iterate
                    // (we have at lease one this._tag in the inner XML fragment)
                    dataChunk += endTag;
                    return;
                }

                this._buffer.push(dataChunk + endTag);
            }

            dataChunk = '';
            closeTagCount = 0;
        }, this);
    }

    var bufferStr = this._buffer.join('');
    this._bufferSize += Buffer.byteLength(bufferStr, ENCODING);

    // First time we are getting there, initialize a new writer
    if (this._writer === null) {
        this.endAndStartFile();
    }

    // The max file size has been reached, close the current writer and initialise a new one for a new chunk file
    if (this._bufferSize >= this._batchSize) {
        this.endAndStartFile();
    }

    this._writer.write(bufferStr);
    this._buffer = [];

    next();
};

/**
 * Override the emit method from the parent Writable class
 * To be able to call the _flush method when the parent class emits the 'emit' event
 *
 * @param {String} evt
 */
XmlStream.prototype.emit = function (evt) {
    if (evt === 'finish') {
        this._flush(function () {
            EventEmitter.prototype.emit.call(this, 'finish');
        }.bind(this));
    } else {
        var args = Array.prototype.slice.call(arguments);
        EventEmitter.prototype.emit.apply(this, args);
    }
};

module.exports = XmlStream;
