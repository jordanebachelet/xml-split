'use strict';

/**
 * Helper toolbox
 * @module helper
 */

/**
 * @type {Integer}
 */
const MB_TO_B_MULTIPLIER = 1000000;
/**
 * @type {Object}
 */
const DEFAULTS = {
    counter: 1,
    pattern: '',
    size: 10 * MB_TO_B_MULTIPLIER, // Default: 10Mb
    removecomments: false
};

/**
 * Parse the counter option
 *
 * @param {Object} opts
 *
 * @returns {Int}
 */
const parseCounter = opts => opts && !isNaN(parseInt(opts.counter, 10)) ? parseInt(opts.counter, 10) : DEFAULTS.counter;
/**
 * Parse the pattern option
 *
 * @param {Object} opts
 *
 * @returns {String}
 */
const parsePattern = opts => opts && opts.pattern !== undefined ? opts.pattern : DEFAULTS.pattern;
/**
 * Parse the size option
 *
 * @param {Object} opts
 *
 * @returns {Int}
 */
const parseSize = opts => opts && !isNaN(parseFloat(opts.size)) ? parseFloat(opts.size) * MB_TO_B_MULTIPLIER : DEFAULTS.size;

/**
 * This method will parse and initialize options from the inputs
 *
 * @alias module:util
 *
 * @param {Object} options The commander options
 *
 * @returns {Object}
 */
module.exports.parseOptions = options => {
    if (options === undefined) {
        return DEFAULTS;
    }

    var opts = Object.assign({}, DEFAULTS);
    opts.counter = parseCounter(options);
    opts.pattern = parsePattern(options);
    opts.size = parseSize(options);
    opts.removecomments = options.removecomments || DEFAULTS.removecomments;

    return opts;
};
