'use strict';

/**
 * Custom logger
 * @module log
 */

/**
 * @type {node_modules/chalk}
 */
const chalk = require('chalk');
/**
 * @type {node_modules/maybe}
 */
const Maybe = require('maybe');
/**
 * @type {node_modules/ramda}
 */
const R = require('ramda');

/**
 * @type {Object}
 */
const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
};
/**
 * @type {Object}
 */
const colors = {
    error: chalk.red,
    info: chalk.black,
    success: chalk.green,
    warn: chalk.yellow
};
/**
 * @type {Object}
 */
const consoleTypes = {
    error: console.error,
    info: console.info,
    success: console.log,
    warn: console.warn
};
/**
 * @type {String}
 */
const locale = 'en-US';

/**
 * Returns true if the current environement {quiet} variable is set to true or 'true'
 *
 * @returns {Boolean}
 */
const isEnvQuiet = () => [true, 'true'].indexOf(process.env.quiet) > -1;
/**
 * Returns true if the current environement {runningAsScript} variable is set to true or 'true'
 *
 * @returns {Boolean}
 */
const runAsScript = () => [true, 'true'].indexOf(process.env.runningAsScript) > -1;
/**
 * Returns true if the tool has to be quiet (meaning not logging everything)
 *
 * @alias module:log
 *
 * @param {Boolean} checkOnlyRunningMode only checks the running mode (not the environment variables)
 *
 * @returns {Boolean}
 */
const isQuiet = (checkOnlyRunningMode = false) => checkOnlyRunningMode ? runAsScript() : isEnvQuiet() || runAsScript();

/**
 * Log the given message for the given type
 *
 * @param {String} type
 *
 * @returns {Maybe}
 */
const log = (type, message) => {
    let d = new Date();
    message = colors[type](d.toLocaleDateString(locale, options), ': ', message);
    consoleTypes[type](message);
    return true;
};

/**
 * This method is used to handle the gateway based on the isQuiet method.
 * If the actual behaviour is quiet, don't log anything, else log the given {message}
 *
 * @param {Boolean} checkOnlyRunningMode
 * @param {String} type
 * @param {Object} message
 *
 * @returns {Maybe}
 */
const callLog = (checkOnlyRunningMode, type, message) => Maybe(isQuiet(checkOnlyRunningMode) ? undefined : log(type, message));

module.exports = {
    isQuiet,
    /**
     * Log the given message as an info message
     *
     * @alias module:log
     *
     * @returns {Maybe}
     */
    info: R.curry(callLog)(false)('info'),
    /**
     * Log the given message as a warn message
     *
     * @alias module:log
     *
     * @returns {Maybe}
     */
    warn: R.curry(callLog)(false)('warn'),
    /**
     * Log the given message as a success message
     *
     * @alias module:log
     *
     * @returns {Maybe}
     */
    success: R.curry(callLog)(true)('success'),
    /**
     * Log the given message as an error message
     *
     * @alias module:log
     *
     * @returns {Maybe}
     */
    error: R.curry(callLog)(true)('error')
};
