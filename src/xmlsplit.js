'use strict';

/**
 * Main XML Splitter handle
 * @module xmlsplitter
 */

/**
 * @type {node_modules/fs}
 */
const fs = require('fs');
/**
 * @type {node_modules/path}
 */
const path = require('path');
/**
 * @type {node_modules/replacestream}
 */
const replacestream = require('replacestream');

/**
 * @type {lib/helper}
 */
const helper = require('../lib/helper');
/**
 * @type {lib/log}
 */
const log = require('../lib/log');
/**
 * @type {lib/streamer}
 */
const XmlStreamer = require('../lib/streamer');

/**
 * @type RegExp
 */
const REGEXP_XML_COMMENT = /<!--[\s\S]+-->/mg;

/**
 * The split method will generate small chuncks of the given {sourceFile} based on the given {options}
 *
 * @alias module:xmlsplitter
 *
 * @param {String} sourceFile The path of the source path
 * @param {String} node The node that represent the large number of items
 * @param {Object} options Options from commander
 *
 * @returns {Promise}
 * @resolve {Integer} If everything goes well, returns the number of chunk files generated
 * @reject {Error} Returns an error if one happened
 */
function split(sourceFile, node, options) {
    return new Promise((resolve, reject) => {
        if (sourceFile === undefined || sourceFile.length === 0 || node === undefined || node.length === 0) {
            reject(new Error('Missing input parameters.'));
            return;
        }

        if (!fs.existsSync(sourceFile)) {
            reject(new Error(`The source file "${sourceFile}" does not exists.`));
            return;
        }

        /**
         * @type {Object}
         */
        var params = helper.parseOptions(options);
        /**
         * @type {String}
         */
        var filepath = path.resolve(process.cwd(), sourceFile).replace(path.basename(sourceFile), '');
        /**
         * @type {String}
         */
        var extention = path.extname(sourceFile);
        /**
         * @type {String}
         */
        var filename = path.basename(sourceFile, extention);

        log.success('Start split process.');

        /**
         * @type {XmlStreamer}
         */
        var xmlStream = new XmlStreamer(params.size, node, filepath, filename, params.pattern, extention, params.counter);

        if (params.removecomments === true) {
            fs.createReadStream(sourceFile)
                .pipe(replacestream(REGEXP_XML_COMMENT, '')) // Removes all comments from the file
                .pipe(xmlStream).on('error', err => {
                    log.error(err);
                    reject(err);
                }).on('done', fileCounter => resolve(fileCounter));
        } else {
            fs.createReadStream(sourceFile)
                .pipe(xmlStream).on('error', err => {
                    log.error(err);
                    reject(err);
                }).on('done', fileCounter => resolve(fileCounter));
        }
    });
}

module.exports.split = split;
