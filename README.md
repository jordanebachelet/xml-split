# XML Split #

[![Coverage Status](https://coveralls.io/repos/bitbucket/jordanebachelet/xml-split/badge.svg?branch=master)](https://coveralls.io/bitbucket/jordanebachelet/xml-split?branch=master)
[![NPM version](https://img.shields.io/npm/v/@jordanebachelet/xml-split.svg)](https://www.npmjs.com/package/@jordanebachelet/xml-split)

## Installation

* Go to the directory where you cloned the repository.
* Run `npm install @jordanebachelet/xml-split`.

## How to use
### Through command line interface
You can run the `npm link` command before using this tool. This will allows you to directly run the `xmlsplit` command in your command line.

Then, you can run the following commands:
```bash
# without npm link
node bin/cli.js split path/to/big/xml/file.xml "child_node" -s 10 -p ".part_" -c 0

# with npm link
xmlsplit split path/to/big/xml/file.xml "child_node" -s 10 -p ".part_" -c 0
```
This command will split the given XML file into chunks of 10 Mb files, renamed as file.part_0.xml, file.part_1.xml, file.part_2.xml ...

### Through the Javascript interface
You can use this tool directly in your NodeJS scripts.

The `split` method returns a native [Promise](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise) so you can do some work after the method.

Hereafter how to integrate this tool in your scripts:
```javascript
const xmlSplit = require('xml-split');

xmlSplit.split('path/to/big/xml/file.xml', 'child_node', {
    size: 10,
    pattern: '.part_',
    counter: 0
}).then(() => {
    console.log('split complete');
});
```
## API Reference

Please check the docs folder [here](https://bitbucket.org/jordanebachelet/xml-split/src/master/docs/).