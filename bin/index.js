'use strict';

/**
 * Javascript interface for the XML Split tool
 * @module index
 */

/**
 * @type {src/xmlsplitter}
 */
const splitter = require('../src/xmlsplit');

/**
 * Javascript interface method to split large XML files
 *
 * @alias module:index
 *
 * @param {String} source The source file path where to find the big XML file to split
 * @param {String} node The node that represent the biggest number of nodes (that we want to move into chunks)
 * @param {Object} options The object that will contains all options that can be passed to the method (to simulate the commnad line options)
 *
 * @returns {Promise}
 * @resolve {Integer} If everything goes well, returns the number of chunk files generated
 * @reject {Error} Returns an error if one happened
 *
 * @example
 * // This command will split the XML file into chunks of 10 Mb files, renamed as file_part_0.xml, file_part_1.xml, file_part_2.xml ...
 * const xmlSplit = require('xmlsplit');
 *
 * xmlSplit.split('path/to/big/xml/file.xml', 'child_node', {
 *     size: 10,
 *     pattern: '_part_',
 *     counter: 0
 * });
 */
module.exports.split = (source, node, options) => splitter.split(source, node, options);
