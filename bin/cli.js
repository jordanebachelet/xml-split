#!/usr/bin/env node

'use strict';

/**
 * Command line interface for the XML Split tool
 *
 * Usage: split [options] <source> <node>
 *
 * Options:
 * *   -s, --size [size]        Size of the target file. The size has to be a Mb number (i.e. "100" will be interpreted as 100Mb). Default: 10.
 * *   -p, --pattern [pattern]  Pattern appended at the end of the file names (before the file extention).
 * *   -c, --counter [counter]  Starting value of the file counter. Default: 1
 * *   -h, --help               output usage information
 *
 * The following examples will split the XML file into chunks of 10 Mb files, renamed as file.part_0.xml, file.part_1.xml, file.part_2.xml ...
 * @example
 * // with node command
 * node bin/cli.js split path/to/big/xml/file.xml child_node -s 10 -p ".part_" -c 0
 *
 * @example
 * // after a "npm link"
 * xmlsplit split path/to/big/xml/file.xml child_node -s 10 -p ".part_" -c 0
 *
 * @module cli
 */

/**
 * @type {Object}
 */
const packageDetails = require('../package');
/**
 * @type {node_modules/commander}
 */
const program = require('commander');

/**
 * @type {lib/log}
 */
const log = require('../lib/log');
/**
 * @type {src/xmlsplitter}
 */
const splitter = require('../src/xmlsplit');

process.title = packageDetails.name;
log.info(packageDetails.name, packageDetails.version);
log.info('');

program.version(packageDetails.version).description(packageDetails.description);

/**
 * Command line interface command to split large XML files
 *
 * @alias module:cli
 *
 * @param {String} split The command name
 * @param {String} source The source file path where to find the big XML file to split
 * @param {String} node The node that represent the biggest number of nodes (that we want to move into chunks)
 */
program.command('split <source> <node>')
    .option('-s, --size [size]', 'Size of the target file. The size has to be a Mb number (i.e. "100" will be interpreted as 100Mb). Default: 10.')
    .option('-p, --pattern [pattern]', 'Pattern appended at the end of the file names (before the file extention).')
    .option('-c, --counter [counter]', 'Starting value of the file counter. Default: 1')
    .option('-r, --removecomments', 'Remove comments from the XML file prior to split it.')
    .action((source, node, options) => {
        Promise.resolve().then(() => splitter.split(source, node, options))
            .then(fileCounter => {
                log.success(`End split process: ${fileCounter} files generated.`);
                process.exit(0);
            })
            .catch(e => {
                log.error(e);
                process.exit(-1);
            });
    });

// parse CLI arguments
program.parse(process.argv);

// output help message if no arguments provided
if (!process.argv.slice(2).length) {
    program.help();
}
